# node-red-contrib-xkeys_button

This is the first of a group of Node-RED nodes to enable access to X-keys physical devices.

It is planned to have a dedicated Node-RED node for each X-key event of interest (button, jog, joystick, etc.). These will communicate, using MQTT, with a lightweight X-keys server, whose sole purpose is to mediate access to any physically attached X-keys devices.

This _xkeys\_button_ node encapsulates button events from any or specific X-keys devices.


## Installation

This node requires xkeys-server version 0.0.17 to be running. Please follow the instructions at the [_xkeys-server_](https://gitlab.com/chris.willing/xkeys-server) development repository to install it or, to upgrade an existing installation, see the [_xkeys-server_ upgrade](https://gitlab.com/chris.willing/xkeys-server#upgrading) instructions.

The _node-red-contrib-xkeys\_button_ node itself is best installed from Node-RED's Palette manager. Go to the Palette manager's Install tab and search for _node-red-contrib-xkeys\_button_; then Install it once found. If not found, press the _Refresh module list_ button (two semicircular arrows) and search again.

When installed, a new _xk button_ will be found in the palette tab in a dedicated _Xkeys_ category.


## Usage

An _xkeys\_button-test_ flow is provided in the examples directory using an _xkeys\_button_ configured to process button events from any Xkeys device. The output feeds a _switch_ node which routes button events according to the received payload's buttonid. Buttonid 1 goes to _switch_ output 1, buttonid 2 to output 2, ..., buttonid 8 to output 8. Each of those outputs feed into debug nodes named b1, b2, ..., b8. Button events can then be monitored in the _Debug messages_ tab. A button event with buttonid 1 will be reported by debug node b1, buttonid 2 by debug node b2 etc.

The full msg.payload of the _xkey\_button_ node is as follows:
```
{ device: NAME, pid: PID, uid: UID, buttonid: BUTTONID, action: TYPE }
```
where
- NAME is an abbreviated name of the source device e.g. XK12JOG, which represents the XK-12 Jog & Shuttle device
- PID is the Product ID of the source device e.g. 1062 for the XK-12 Jog & Shuttle device
- UID is the Unit ID of the source device, typically 0 from the factory but assignable 0-255
- BUTTONID is the button number which caused the event
- TYPE is the type of event that occurred. Button events may be either _down_ or _up_ types.

With this output from the _xkeys\_button_ node, the _switch_ node in the example flow is able to use the _msg.payload.buttonid_ field to route each event to the corresponding output.


## Issues

When configuring the node, a drop down list of possible devices shoud be available. If not, instead displaying just a note to `Press Deploy button to see device list`, first follow that advice. If that is unsuccessful (still no list of possible devices), then check the status of the xkeys-server by running the command `sudo systemctl status xkeys-server` in a terminal. Typical output of a normally running xkeys-server will be:
```
pi@pi3b:~ $ sudo systemctl status xkeys-server
● xkeys-server.service - Run xkeys-server as background service
Loaded: loaded (/etc/systemd/system/xkeys-server.service; enabled; vendor preset: enabled)
Active: active (running) since Thu 2021-10-14 09:31:55 AEST; 23h ago
```
whereas a non-running xkeys-server will show something like:
```
pi@pi3b:~ $ sudo systemctl status xkeys-server
● xkeys-server.service - Run xkeys-server as background service
Loaded: loaded (/etc/systemd/system/xkeys-server.service; enabled; vendor preset: enabled)
Active: failed (Result: timeout) since Fri 2021-10-15 08:41:37 AEST; 19s ago
```
If necessary, (re)start the xkeys-server with `sudo systemctl restart xkeys-server`


## Authors and acknowledgment
Many thanks to [P.I. Engineering](https://xkeys.com/) for the donation of several X-keys devices for development and testing.

## License
MIT
