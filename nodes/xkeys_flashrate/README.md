# node-red-contrib-xkeys_flashrate

This is another in a collection of Node-RED nodes which enable access to [X-keys](https://xkeys.com/) physical devices.

It is planned to have a dedicated Node-RED node for each X-key event of interest (button, jog, joystick, etc.). These will communicate, using MQTT, with a lightweight [X-keys server](https://gitlab.com/chris.willing/xkeys-server), whose sole purpose is to mediate access to any physically attached X-keys devices.

This _xkeys\_flashrate_ node configures the LED flashing rate of any _xkeys\_led and _xkeys\_backlight_ nodes.


## Installation

This node requires _xkeys-server_ version 0.0.17 to be running. Please follow the instructions at the [_xkeys-server_](https://gitlab.com/chris.willing/xkeys-server) development repository to install it or, to upgrade an existing installation, see the [_xkeys-server_ upgrade](https://gitlab.com/chris.willing/xkeys-server#upgrading) instructions.

The _node-red-contrib-xkeys\_flashrate_ node itself is best installed from Node-RED's Palette manager. Go to the Palette manager's Install tab and search for _node-red-contrib-xkeys\_flashrate_; then Install it once found. If not found, press the _Refresh module list_ button (two semicircular arrows) and search again.

When installed, a new _xk LED rate_ node will be found in the palette tab in the dedicated _Xkeys_ category.


## Usage

The node's configuration editor can be used to set the frequency of LED flashing for any _xkeys\_flash_ or  _xkeys\_backlight_ node. A setting of 1 produces the fastest flashing while a setting of 255 produces the slowest. In addition, the usual configurable settings to target specific devices by their PID and/or UID are available.

This node is also able to be activated by preceding nodes connected the input. Such preceding nodes must include, at least, a _flashRate_ field in its msg.payload. If the msg.payload also includes optional _pid_list_ or _uid_ fields, these will override the _xkeys\_flashrate_ node's configured settings i.e.
```
    {"flashRate": RATE, "pid_list": PID_LIST,"uid": UID}
```
An example flow is provided in the _examples_ directory to demonstrate possible usage of the _xkeys\_flashrate_ node.
<p align="center" width="100%" ><img width="70%" src="examples/xkeys_flashrate-test.png" ></p>

The lower section just enables buttons 1 & 3 of any connected device to start & stop flashing the device's green led, depending on the _xkeys\_flashrate_ in the upper section, which is configured to set the flash rate to 50 (a somewhat slow rate). However the flashrate may be altered at any time by activating the preceding _Inject_ node (labelled _set flash rate_) which is set to 10 (noteably faster than 50) in this example flow.


## Authors and acknowledgment
Many thanks to [P.I. Engineering](https://xkeys.com/) for the donation of several X-keys devices for development and testing.

## License
MIT
