
module.exports = function(RED) {
	var mqtt = require('mqtt');
	const connectUrl = 'mqtt://localhost';
	const qos = 0;

	var httpAdminDataProducts = {};
	var httpAdminDataDevices = {};
	var httpAdminDataFlashRate = {};
	
	function XkeysFlashRate(config) {
		RED.nodes.createNode(this, config);
		var node = this;
		node.config = config;
		//node.log("node.config = " + JSON.stringify(node.config));
		node.log("myId = " + node.config.id);

		var client = mqtt.connect(connectUrl);
		client.on('reconnect', (error) => {
			node.log('reconnecting:', error)
		})
		client.on('error', (error) => {
			node.log('Connection failed:', error)
		})
		client.on('connect', () => {
			node.log('connected')
			client.subscribe({'/xkeys/server/#':{qos:qos}}, function (err, granted) {
				if (!err) {
					node.log("Subscribed OK, granted: " + JSON.stringify(granted));
					client.publish('/xkeys/node', '{"request":"productList"}')
					client.publish('/xkeys/node/xkeys_flashrate', '{"request":"deviceList"}')
				} else {
					node.log('Subscription failed: ' + err)
				}
			})
		})
		client.on('close', () => {
			node.log("connection closed");
		})
		client.on('message', (topic, message) => {
			//node.log(`received topic：${topic}, msg: ${message.toString()}`);
			var message_obj = "";
			try {
				message_obj = JSON.parse(message);
				//node.log("SID = " + message_obj.server_id);
				if (message_obj.request == "hello") {
					console.log(`Hello from xkeys_server at ${message_obj.server_id} - must have just (re)started `);
				}
				else if (message_obj.request == "result_deviceList") {
					// data should be a dict of info objects keyed by device's uniqueId
					var deviceList = message_obj.data;
					httpAdminDataDevices = deviceList;

					// Based on attached deviceList, are we connected?
					var pid_list = this.config.pid_list || "[]";
					if (device_connected(JSON.parse(pid_list), this.config.unit_id, this.config.duplicate_id)) {
						node.status( {fill:"green",shape:"dot",text:"connected"} );
					} else {
						node.status( {fill:"red",shape:"ring",text:"disconnected"} );
					}
				}
				else if (message_obj.request == "result_productList") {
					// data should be a dict of product objects
					var productList = message_obj.data;
					httpAdminDataProducts = productList;
				}
				else if (message_obj.request == "result_productList") { }
				else if (message_obj.request == "result_deviceList") { }
				else if (message_obj.request == "device_event") { }
				else if (message_obj.request == "heartbeat") {}
				else {
					node.log('FlashRate received unhandled request: ' + message_obj.request);
				}
			}
			catch (e) {
				node.log('ERROR parsing message: ' + e);
			}
		})

		/*
	    * Input messages
		* We expect a msg.payloads of at least:
		*	{"flashRate": RATE}
		* in which case the node's configured pid_list & uinitid values are used
		*
		* The usual (internally configured) msg.payload will have:
		*	{"flashRate": RATE, "pid_list": PID_LIST,"unit_id": UID}
		*/
		node.on('input', function(msg) {
			var mkeys = Object.keys(msg.payload);
			//console.log("mkeys: " + JSON.stringify(mkeys));
			//node.log("msg.payload: " + JSON.stringify(msg.payload));
			if (mkeys.includes("flashRate")) {
				// Check that flashRate is OK
				if( (isNaN(parseInt(msg.payload.flashRate))) || (parseInt(msg.payload.flashRate) < 1) || (parseInt(msg.payload.flashRate) > 255) ) {
					node.log("Received bad flashRate: " + msg.payload.flashRate);
					return;
				}

				var request_pid_list = "[]";
				var request_uid = "";
				var duplicate_id = "";

				if (mkeys.includes("pid_list")) {
					request_pid_list = msg.payload.pid_list;
				} else {
					request_pid_list = node.config.pid_list;
				}

				if (mkeys.includes("unit_id")) {
					request_uid = msg.payload.unit_id;
				} else {
					request_uid = node.config.unit_id;
				}

				if (mkeys.includes("duplicate_id")) {
					duplicate_id = msg.payload.duplicate_id;
				} else {
					duplicate_id = node.config.duplicate_id;
				}

				// Ask server to activate the led
				/*
				*	Message format:
				*	{request: "method", pid_list:[e0,e1,...,eN], unit_id:UID, duplicate_id:ORDER, name:METHODNAME, params: [p0,p1,...,pN]}
				*	where p1 is the flashRate set (p0, empty array, is unused)
				*/
				var method_request;
				method_request = {	"request": "method",
									"pid_list"     : JSON.parse(request_pid_list),
									"unit_id"      : request_uid.length==0?-1:parseInt(request_uid),
									"duplicate_id" : duplicate_id.length==0?-1:parseInt(duplicate_id),
									"name"         : "setFlashRate",
									"params"       : [[], msg.payload.flashRate]
								};
				client.publish('/xkeys/node/xkeys_flashrate', JSON.stringify(method_request));

			} else {
				console.log("Not interested in this msg (no \"flashRate\" field)");
			}
		})

		this.on('close', function(done) {
			client.end();
			done();
		})

		// Does any attached device match specified pids, unit_id & dup_id ?
		// pids: array of possible PIDs for a device (empty => ANY)
		// unit_id: unitId of a device
		// dup_id:  duplicate_id of a device
		function device_connected(...deviceArgs) {
			const pids = deviceArgs[0];
			const unit_id = deviceArgs[1];
			const dup_id = deviceArgs[2];
			const devs = Object.keys(httpAdminDataDevices);
			var device_matched = false;
			var regex_string = ""
			var regex;

			if (pids.length == 0) {
				//	No product_ids provided => ANY product_id
				regex_string = regex_string + "\[0-9\]+-";
				if (unit_id) {
					regex_string = regex_string + unit_id + "-";
				} else {
					regex_string = regex_string + "\[0-9\]+-";
				}
				if (dup_id) {
					regex_string = regex_string + dup_id;
				} else {
					regex_string = regex_string + "\[0-9\]+";
				}
				regex = new RegExp(regex_string);
				devs.forEach( (item) => {
					if (regex.test(item)) { device_matched = true; }
				})
			} else {
				//	An array of endpoints provided
				pids.forEach(function (item) { 
					regex_string = ""
					regex_string = regex_string + item + "-";
					if (unit_id) {
						regex_string = regex_string + unit_id + "-";
					} else {
						regex_string = regex_string + "\[0-9\]+-";
					}
					if (dup_id) {
						regex_string = regex_string + dup_id;
					} else {
						regex_string = regex_string + "\[0-9\]+";
					}
					regex = new RegExp(regex_string);
					devs.forEach( (item) => {
						if (regex.test(item)) { device_matched = true; }
					})
				})
			}
			return device_matched;

		}	// function device_connected

	}	// function XkeysFlashRate

	RED.nodes.registerType("xkeys_flashrate", XkeysFlashRate);

	RED.httpAdmin.get("/xkeys_flashrate/products", function (req, res) {
		res.json(httpAdminDataProducts);
	});
	RED.httpAdmin.get("/xkeys_flashrate/devices", function (req, res) {
		res.json(httpAdminDataDevices);
	});

	RED.httpAdmin.post("/xkeys_flashrate_inject/:id", RED.auth.needsPermission("xkeys_flashrate_inject.write"), function(req,res) {
		console.log("posting something: " + JSON.stringify(req.body));
		var node = RED.nodes.getNode(req.params.id);
		if (node != null) {
			try {
				if (req.body) {
					node.receive(req.body);
				} else {
					node.receive();
				}
				res.sendStatus(200);
			}
			catch (err) {
				res.sendStatus(500);
				node.error(RED._("inject.failed",{error:err.toString()}));
			}
		} else {
			console.log("bad post");
			res.sendStatus(404);
		}
	});

}
/*
	Node colours:
	Light blue is: R:  93, G: 200, B: 244 (#5dc8f4)
	Dark blue is:  R:  78, G: 169, B: 242 (#4ea8f2)
*/

