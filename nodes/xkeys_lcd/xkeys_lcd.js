
module.exports = function(RED) {
	var mqtt = require('mqtt');
	const connectUrl = 'mqtt://localhost';

	var httpAdminDataProducts = {};
	var httpAdminDataDevices = {};
	var httpAdminDataBacklightOn = true;

	const defaultText = "Clockwork Sport"
	var textLines = [defaultText,""];
	
	function XkeysLCD(config) {
		RED.nodes.createNode(this, config);
		var node = this;
		node.config = config;
		//node.log("node.config = " + JSON.stringify(node.config));
		node.log("myId = " + node.config.id);
		var flowContext = this.context().flow;

		var client = mqtt.connect(connectUrl);
		client.on('reconnect', (error) => {
			node.log('reconnecting:', error)
		})
		client.on('error', (error) => {
			node.log('Connection failed:', error)
		})
		client.on('connect', () => {
			node.log('connected')
			client.subscribe('/xkeys/server/#', function (err, granted) {
				if (!err) {
					node.log("Subscribed OK, granted: " + JSON.stringify(granted));
					client.publish('/xkeys/node', '{"request":"productList"}')
					client.publish('/xkeys/node', '{"request":"deviceList"}')
				} else {
					node.log('Subscription failed: ' + err)
				}
			})
		})
		client.on('close', () => {
			node.log("connection closed");
		})
		client.on('message', (topic, message) => {
			//node.log(`received topic：${topic}, msg: ${message.toString()}`);
			var message_obj = "";
			try {
				message_obj = JSON.parse(message);
				//node.log("SID = " + message_obj.server_id);
				if (message_obj.request == "hello") {
					console.log(`Hello from xkeys_server at ${message_obj.server_id} - must have just (re)started `);
					// In case xkeys-server restarted with updated devices/product list
					client.publish('/xkeys/node', '{"request":"productList"}')
					client.publish('/xkeys/node', '{"request":"deviceList"}')
				}
				else if (message_obj.request == "result_deviceList") {
					// data should be a dict of info objects keyed by device's uniqueId
					var deviceList = message_obj.data;
					httpAdminDataDevices = deviceList;

					var pid_list = this.config.pid_list || "[]";
					if (device_connected(JSON.parse(pid_list), this.config.unit_id, this.config.duplicate_id)) {
						node.status( {fill:"green",shape:"dot",text:"connected"} );
					} else {
						node.status( {fill:"red",shape:"ring",text:"disconnected"} );
						textLines = [defaultText,""];
					}
				}
				else if (message_obj.request == "result_productList") {
					// data should be a dict of product objects
					var productList = message_obj.data;
					httpAdminDataProducts = productList;
				}
				else if (message_obj.request == "result_flashRate") { }
				else if (message_obj.request == "device_event") { }
				else if (message_obj.request == "heartbeat") {}
				else {
					node.log('LCD received unhandled request: ' + message_obj.request);
				}
			}
			catch (e) {
				node.log('ERROR parsing message: ' + e);
			}
		})

		/*
	    * Input messages
		* We expect msg.payload of various styles:
		* msg.payload = { lcdtext: [line1Text, line2Text], backlight: TRUEFALSE, unit_id: UID }
		* 	where only lcdtext is required, the others optional
		*
		* msg.payload = TEXTSTRING
		*	TEXT as a string
		*
		* msg.payload = TEXTNUMBER
		*	TEXT as a number
		*/
		node.on('input', function(incoming) {
			//console.log(`Incoming: ${JSON.stringify(incoming)}`);

			/*	This msg object what is ultimately to feed method parameters
			*	We build it up from the input received here.
			*/
			var msg = {"payload":{}};

			/*
			*	We assume the LCD device has 2 lines.
			*	This is true right now when the only such device is the XK16LCD.
			*/
			var default_linenum;
			if (node.config.linenum) {
				default_linenum = node.config.linenum - 1;
			} else {
				default_linenum = 1; // The second line
			}

			/*	The simple payloads (just a string or number) don't prefer any linenum,
			*	so just use the configured default linenum
			*/
			if (typeof(incoming.payload) == "number") {
				textLines[default_linenum] = incoming.payload.toString();
				msg["payload"] = {"lcdtext": textLines};

			} else if (typeof(incoming.payload) == "string") {
				textLines[default_linenum] = incoming.payload;
				msg["payload"] = {"lcdtext": textLines};

			} else {
				/*	If the payload is an object, it may contain linenum, unit_id and/or backlight preferences.
				*	However, only the lcdtext property is mandatory.
				*/
				if (!incoming.payload.hasOwnProperty("lcdtext")) { return; }

				var incoming_text;
				if (typeof(incoming.payload.lcdtext) == "number") {
					incoming_text = incoming.payload.lcdtext.toString();
				} else if (typeof(incoming.payload.lcdtext) == "string") {
					incoming_text = incoming.payload.lcdtext;
				} else {
					console.log("Bad lcd text format");
					return;
				}

				// Does it contain a line number preference?
				var line_number;
				if (incoming.payload.hasOwnProperty("linenum")) {
					line_number = incoming.payload.linenum - 1;
				} else {
					line_number = default_linenum;	// already zero based
				}

				// Does it contain a backlight instruction?
				if (! incoming.payload.hasOwnProperty("backlight")) {
					// Just transfer the text to textLines, ready for dispatch
					textLines[line_number] = incoming_text;
				} else {
					//	In this case, interpret text "" as "leave alone"
					//	i.e. use previous text and only update textLines if lcdtext has something to give
					if (incoming_text.length > 0) {
						textLines[line_number] = incoming_text
					}
					msg.payload["backlight"] = incoming.payload.backlight;
				}
				msg.payload["lcdtext"] = textLines;
			}

			var mkeys = Object.keys(msg.payload);
			if (mkeys.includes("lcdtext")) {
				//console.log(`Received text (${msg.payload.lcdtext})`);

				var backlight = flowContext.get("backlight_on") || true;
				if (mkeys.includes("backlight")) {
					backlight = msg.payload.backlight;
				} else if (node.config.backlight ) {
					backlight = (node.config.backlight.toLowerCase() == "true");
				}
				flowContext.set("backlight_on", backlight);
				httpAdminDataBacklightOn = backlight;

				/*
				*	If both msg.payload and node.config have configured pid_list,
				*	they need to match.
				*	If neither has configured pid_list, then
				*	what is the target? All connected devices or none of them?
				*/
				var pid_list = []; // The ones we end up using
				var configured_pid_list = JSON.parse(this.config.pid_list) || [];
				var msg_pid_list;	// What we may have been sent
				if (configured_pid_list.length == 0 ) {
					// This node has no device pid_list configured
					// so use whatever is requested in the msg.
					//console.log("No pid_list configured by this node");

					if (mkeys.includes("pid_list")) {
						// No pid_list from node
						// Some pid_list requested by msg
						msg_pid_list = msg.payload.pid_list;
						pid_list = msg_pid_list;
					} else {
						// No pid_list from config
						// No pid_list from msg
						// Therefore do nothing
						pid_list.push(-1);
					}
				} else {
					//console.log("Configured pid_list: " + JSON.stringify(configured_pid_list));
					if (mkeys.includes("pid_list")) {
						// pid_list from config
						// pid_list from msg
						// Need to arbitrate - or just ignore pid_list from msg?
						var configSet = new Set(JSON.parse(this.config.pid_list));
						if ( (msg.payload.pid_list.filter(function(x) { return configSet.has(x); })).length == 0 ) {
							// Nothing matched
							//console.log("msg pid_list don't match configured pid_list. Not proceeding");
							pid_list.push(-1);
						} else {
							//console.log("msg pid_list included in configured pid_list");
							pid_list = JSON.parse(this.config.pid_list);
						}
					} else {
						// pid_list from config
						// No pid_list from msg
						// Use configured pid_list
						pid_list = JSON.parse(this.config.pid_list);
					}
				}
				if (pid_list.includes(-1)) {
					console.log("No suitable pid_list specified");
					return
				}

				/*
				*	If a unit_id has been configured, it takes precedence over any requested unit_id
				*/
				var unit_id = "";
				if (node.config.unit_id) {
					unit_id = this.config.unit_id;
					if (mkeys.includes("unit_id")) {
						if (msg.payload.unit_id != unit_id) {
							// No match
							return;
						}
					}
				} else if (mkeys.includes("unit_id")) {
					//console.log("typeof unit_id = " + typeof(msg.payload.unit_id));
					unit_id = msg.payload.unit_id;
				}

				/*
				*	If a duplicate_id has been configured, it takes precedence over any requested duplicate_id
				*/
				var duplicate_id = "";
				if (node.config.duplicate_id) {
					duplicate_id = this.config.duplicate_id;
					if (mkeys.includes("duplicate_id")) {
						if (msg.payload.duplicate_id != duplicate_id) {
							// No match
							return;
						}
					}
				} else if (mkeys.includes("duplicate_id")) {
					//console.log("typeof duplicate_id = " + typeof(msg.payload.duplicate_id));
					duplicate_id = msg.payload.duplicate_id;
				}


				// Ask server to activate the lcd
				/*
				*	Message format:
				*	{request: "method", pid_list:[e0,e1,e2,...,eN], unit_id:UID, duplicate_id:ORDER, name:METHODNAME, params: [p0,p1,...,pN]}
				*	where p0 is an array of the text strings to be displayed [line1Text,line2Text]
				*	      p1 is a boolean requesting backlight be on or off
				*/
				var method_request;
				method_request = {	"request"      : "method",
									"pid_list"     : pid_list,
									"unit_id"      : unit_id.length==0?-1:parseInt(unit_id),
									"duplicate_id" : duplicate_id.length==0?-1:parseInt(duplicate_id),
									"name"         : "writeLcdDisplay",
									"params"       : [msg.payload.lcdtext, backlight]
								};
				//console.log(`XXXX ${JSON.stringify(method_request)}`);
				client.publish('/xkeys/node', JSON.stringify(method_request));

			} else {
				console.log("Not interested in this msg (no \"lcdtext\" field)");
			}
		})

		this.on('close', function(done) {
			client.end();
			done();
		})

		// Does any attached device match specified pids, unit_id & dup_id ?
		// pids: array of possible PIDs for a device (empty => ANY)
		// unit_id: unitId of a device
		// dup_id:  duplicate_id of a device
		 function device_connected(...deviceArgs) {
			const pids = deviceArgs[0];
			const unit_id = deviceArgs[1];
			const dup_id = deviceArgs[2];
			const devs = Object.keys(httpAdminDataDevices);
			var device_matched = false;
			var regex_string = ""
			var regex;

			if (pids.length == 0) {
				//	No product_ids provided => ANY product_id
				regex_string = regex_string + "\[0-9\]+-";
				if (unit_id) {
					regex_string = regex_string + unit_id + "-";
				} else {
					regex_string = regex_string + "\[0-9\]+-";
				}
				if (dup_id) {
					regex_string = regex_string + dup_id;
				} else {
					regex_string = regex_string + "\[0-9\]+";
				}
				regex = new RegExp(regex_string);
				devs.forEach( (item) => {
					if (regex.test(item)) { device_matched = true; }
				})
			} else {
				//	An array of endpoints provided
				pids.forEach(function (item) { 
					regex_string = ""
					regex_string = regex_string + item + "-";
					if (unit_id) {
						regex_string = regex_string + unit_id + "-";
					} else {
						regex_string = regex_string + "\[0-9\]+-";
					}
					if (dup_id) {
						regex_string = regex_string + dup_id;
					} else {
						regex_string = regex_string + "\[0-9\]+";
					}
					regex = new RegExp(regex_string);
					devs.forEach( (item) => {
						if (regex.test(item)) { device_matched = true; }
					})
				})
			}
			return device_matched;

		}	// function device_connected

	}	// function XkeysLCD

	RED.nodes.registerType("xkeys_lcd", XkeysLCD);

	RED.httpAdmin.get("/xkeys_lcd/products", function (req, res) {
		res.json(httpAdminDataProducts);
	});
	RED.httpAdmin.get("/xkeys_lcd/devices", function (req, res) {
		res.json(httpAdminDataDevices);
	});
	RED.httpAdmin.get("/xkeys_lcd/backlight_on", function (req, res) {
		res.json(httpAdminDataBacklightOn);
	});

}
/*
	Node colours:
	Light blue is: R:  93, G: 200, B: 244 (#5dc8f4)
	Dark blue is:  R:  78, G: 169, B: 242 (#4ea8f2)
*/

