# node-red-contrib-xkeys_lcd

This is another in a collection of Node-RED nodes which enable access to [X-keys](https://xkeys.com/) physical devices.

It is planned to have a dedicated Node-RED node for each X-key event of interest (button, jog, joystick, etc.). These will communicate, using MQTT, with a lightweight [X-keys server](https://gitlab.com/chris.willing/xkeys-server), whose sole purpose is to mediate access to any physically attached X-keys devices.

This _xkeys\_lcd_ node encapsulates actions for any or specific LCD equiped X-keys devices.


## Installation

This node requires _xkeys-server_ version 0.9.0 to be running. Please follow the instructions at the [_xkeys-server_](https://gitlab.com/chris.willing/xkeys-server) development repository to install it or, to upgrade an existing installation, see the [_xkeys-server_ upgrade](https://gitlab.com/chris.willing/xkeys-server#upgrading) instructions.

The _node-red-contrib-xkeys\_lcd_ node itself is best installed from Node-RED's Palette manager. Go to the Palette manager's Install tab and search for _node-red-contrib-xkeys\_lcd_; then Install it once found. If not found, press the _Refresh module list_ button (two semicircular arrows) and search again.

When installed, a new _xk LCD_ node will be found in the palette tab in the dedicated _Xkeys_ category.


## Usage

The node's configuration editor can be used to set the target Xkeys device and Unit ID, as well as the default line number to write to.

The msg.payload expected by the _xkey\_lcd_ node is as follows:
```
{ lcdtext: TEXT, linenum: LINENUM, backlight: TRUEFALSE, uid: UID }
```
where
- TEXT is the string to display
- LINENUM is 1 or 2, denoting which display line to write to.
- backlight property is one of _true_ or _false_ to request the backlight to be on or off respectively
- UID is the Unit ID to target. If absent or set to "", any available unit is targeted.

This node will also accept a plain text string or number as the input msg.payload, in which case the payload is displayed on whichever line number the node is configured for.

An example flow is provided in the examples directory to demonstrate a number of methods to activate the xkeys_lcd node.
<p align="center" width="100%" ><img width="70%" src="examples/xkeys_lcd-test.png" ></p>

- the first two Inject nodes send "Node-RED" to line 1 and "Hello World!" to line 2 of the display.
- the next two Inject nodes send a number (timestamp) and text string ("This is the end") as their respective msg.payloads, which will be displayed on whichever line has been selected in the node configuration (default is line 2).
- the button and _CLR LCD_ function nodes combine to clear both lines of the display. Any button could be used to trigger the action but in this case the function node enables only a button_id of 13 to pass as this is the button_id of the _Clear_ button of the XK-16 LCD device.
- the same button node combines with the _LCD backlight_ node to toggle the backlight of the display. In this case the function node filters buttonid 14 (the _?_ key of the XK-16 LCD) to toggle the backlight.
- the _JogShuttle_ and _Update display_ nodes demonstrate live jog & shuttle updates to the LCD display.
- the next three Inject nodes show how to clear all or individual lines of the display.
- the last two Inject nodes show how to turn the display backlight on or off.


## Issues

When configuring the node, a drop down list of possible devices shoud be available. If not, instead displaying just a note to `Press Deploy button to see device list`, first follow that advice. If that is unsuccessful (still no list of possible devices), then check the status of the xkeys-server by running the command `sudo systemctl status xkeys-server` in a terminal. Typical output of a normally running xkeys-server will be:
```
pi@pi3b:~ $ sudo systemctl status xkeys-server
● xkeys-server.service - Run xkeys-server as background service
Loaded: loaded (/etc/systemd/system/xkeys-server.service; enabled; vendor preset: enabled)
Active: active (running) since Thu 2021-10-14 09:31:55 AEST; 23h ago
```
whereas a non-running xkeys-server will show something like:
```
pi@pi3b:~ $ sudo systemctl status xkeys-server
● xkeys-server.service - Run xkeys-server as background service
Loaded: loaded (/etc/systemd/system/xkeys-server.service; enabled; vendor preset: enabled)
Active: failed (Result: timeout) since Fri 2021-10-15 08:41:37 AEST; 19s ago
```
If necessary, (re)start the xkeys-server with `sudo systemctl restart xkeys-server`


## Authors and acknowledgment
Many thanks to [P.I. Engineering](https://xkeys.com/) for the donation of several X-keys devices for development and testing.

## License
MIT
