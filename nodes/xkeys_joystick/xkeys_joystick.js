
module.exports = function(RED) {
	var mqtt = require('mqtt');
	const connectUrl = 'mqtt://localhost';
	const qos = 0;	// Connection to local broker should be good

	var httpAdminDataProducts = {};
	var httpAdminDataDevices = {};
	
	function XkeysJoystick(config) {
		RED.nodes.createNode(this, config);
		var node = this;
		node.config = config;
		//node.log("node.config = " + JSON.stringify(node.config));
		//node.log("myId = " + node.config.id);

		var client = mqtt.connect(connectUrl);
		client.on('reconnect', (error) => {
			node.log('reconnecting:', error)
		})
		client.on('error', (error) => {
			node.log('Connection failed:', error)
		})
		client.on('connect', () => {
			node.log('connected')
			//client.subscribe('/xkeys/server/#', function (err, granted) {
			client.subscribe({'/xkeys/server/#':{qos:qos}}, function (err, granted) {
				if (!err) {
					node.log("Subscribed OK, granted: " + JSON.stringify(granted));
					client.publish('/xkeys/node', '{"request":"productList"}')
					client.publish('/xkeys/node', '{"request":"deviceList"}')
				} else {
					node.log('Subscription failed: ' + err)
				}
			})
		})
		client.on('close', () => {
			node.log("connection closed");
		})
		client.on('message', (topic, message) => {
			//node.log(`received topic：${topic}, msg: ${message.toString()}`);
			var message_obj = "";
			try {
				message_obj = JSON.parse(message);
				//node.log("SID = " + message_obj.server_id);
				if (message_obj.request == "hello") {
					console.log(`Hello from xkeys_server at ${message_obj.server_id} - must have just (re)started `);
					// In case xkeys-server restarted with updated devices/product list
					client.publish('/xkeys/node', '{"request":"productList"}')
					client.publish('/xkeys/node', '{"request":"deviceList"}')
				}
				else if (message_obj.request == "result_deviceList") {
					// data should be a dict of info objects keyed by device's uniqueId
					var deviceList = message_obj.data;
					httpAdminDataDevices = deviceList;

					var pid_list = this.config.pid_list || "[]";
					if (device_connected(JSON.parse(pid_list), this.config.unit_id, this.config.duplicate_id)) {
						node.status( {fill:"green",shape:"dot",text:"connected"} );
					} else {
						node.status( {fill:"red",shape:"ring",text:"disconnected"} );
					}
				}
				else if (message_obj.request == "result_productList") {
					// data should be a dict of product objects
					var productList = message_obj.data;
					httpAdminDataProducts = productList;
				}
				else if (message_obj.request == "device_event") {
					//console.log("Event " + JSON.stringify(message_obj));
					var topic_elements = topic.split("/");
					etype = topic_elements[3];
					/*	For joystick node,
					*	topic_elements[0] is nothing
					*	topic_elements[1] is "xkeys"
					*	topic_elements[2] is "server"
					*	topic_elements[3] is "joystick_event" (etype)
					*	topic_elements[4] is product_id
					*	topic_elements[5] is unit_id
					*	topic_elements[6] is duplicate_id
					*	topic_elements[7] is controller_id # (index)
					*/
					if (etype == "joystick_event") {
						//console.log("JOYSTICK event");

						// Check that PID & UID are what we're interested in
						var pid_list = this.config.pid_list || "[]";
						var pids = JSON.parse(pid_list);
						if (pids.length == 0 || pids.includes(parseInt(topic_elements[4]))) {
							// Either no PID was configured i.e. any PID will do
							// or this event is for a PID that has been configured

							// Now filter Controller #
							if (topic_elements[6] == this.config.controller_id || this.config.controller_id == "") {
								// Either this event was for a configured controller_id or no controller_id was configured

								// Now filter Unit #
								if (topic_elements[5] == this.config.unit_id || this.config.unit_id == "") {
									// Either event was for a configured unit_id or no unit_id was configured

									// Now filter Duplicate #
									if (topic_elements[6] == this.config.duplicate_id || this.config.duplicate_id == "") {
										// Either event was for a configured duplicate_id or no duplicate_id was configured

										// Prepare output msg
										var msg = {};
										msg["payload"] = {
											"device"       : message_obj.data.shortnam,
											"product_id"   : topic_elements[4],
											"unit_id"      : topic_elements[5],
											"duplicate_id" : topic_elements[6],
											"controller_id": topic_elements[7],
											"x"            : message_obj.data.position.x,
											"y"            : message_obj.data.position.y,
											"z"            : message_obj.data.position.z,
											"deltaZ"       : message_obj.data.position.deltaZ
										};
										node.send(msg);
									}
								}
							}
						}
					}
				}
				else if (message_obj.request == "result_flashRate") {}
				else if (message_obj.request == "heartbeat") { }
				else {
					node.log('Received unhandled request: ' + message_obj.request);
				}
			}
			catch (e) {
				node.log('ERROR parsing message: ' + e);
			}
		})

		/*
		// Removed for production
	    // Input messages
		node.on('input', function(msg) {
			if (msg.payload == "close") {
				console.log("close requested");
				client.end();
			} else if (msg.payload == "refresh") {
				console.log("Refresh requested");
				client.publish('/xkeys/node', '{"request":"deviceList"}')
			}
		})
		*/

		this.on('close', function(done) {
			client.end();
			done();
		})

		// Does any attached device match specified pids, unit_id & dup_id ?
		// pids: array of possible PIDs for a device (empty => ANY)
		// unit_id: unitId of a device
		// dup_id:  duplicate_id of a device
		function device_connected(...deviceArgs) {
		//function device_connected (pids, unit_id, dup_id) {
			const pids = deviceArgs[0];
			const unit_id = deviceArgs[1];
			const dup_id = deviceArgs[2];
			const devs = Object.keys(httpAdminDataDevices);
			var device_matched = false;
			var regex_string = ""
			var regex;

			if (pids.length == 0) {
				//	No product_ids provided => ANY product_id
				regex_string = regex_string + "\[0-9\]+-";
				if (unit_id) {
					regex_string = regex_string + unit_id + "-";
				} else {
					regex_string = regex_string + "\[0-9\]+-";
				}
				if (dup_id) {
					regex_string = regex_string + dup_id;
				} else {
					regex_string = regex_string + "\[0-9\]+";
				}
				regex = new RegExp(regex_string);
				devs.forEach( (item) => {
					if (regex.test(item)) { device_matched = true; }
				})
			} else {
				//	An array of endpoints provided
				pids.forEach(function (item) { 
					regex_string = ""
					regex_string = regex_string + item + "-";
					if (unit_id) {
						regex_string = regex_string + unit_id + "-";
					} else {
						regex_string = regex_string + "\[0-9\]+-";
					}
					if (dup_id) {
						regex_string = regex_string + dup_id;
					} else {
						regex_string = regex_string + "\[0-9\]+";
					}
					regex = new RegExp(regex_string);
					devs.forEach( (item) => {
						if (regex.test(item)) { device_matched = true; }
					})
				})
			}
			return device_matched;

		}	// function device_connected

	}	// function XkeysJoystick

	RED.nodes.registerType("xkeys_joystick", XkeysJoystick);

	RED.httpAdmin.get("/xkeys_joystick/products", function (req, res) {
		res.json(httpAdminDataProducts);
	});
	RED.httpAdmin.get("/xkeys_joystick/devices", function (req, res) {
		res.json(httpAdminDataDevices);
	});

}
/*
	Node colours:
	Light blue is: R:  93, G: 200, B: 244 (#5dc8f4)
	Dark blue is:  R:  78, G: 169, B: 242 (#4ea8f2)
*/

