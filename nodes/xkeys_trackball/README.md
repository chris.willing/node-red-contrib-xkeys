# node-red-contrib-xkeys_trackball

This is another of a group of Node-RED nodes which enable access to [X-keys](https://xkeys.com/) physical devices.

It is planned to have a dedicated Node-RED node for each X-key event of interest (button, jog, joystick, etc.). These will communicate, using MQTT, with a lightweight [X-keys server](https://gitlab.com/chris.willing/xkeys-server), whose sole purpose is to mediate access to any physically attached X-keys devices.

This _xkeys\_trackball_ node encapsulates trackball events from any or specific X-keys devices.


## Installation

This node requires _xkeys-server_ version 0.0.19 to be running. Please follow the instructions at the [_xkeys-server_](https://gitlab.com/chris.willing/xkeys-server) development repository to install it or, to upgrade an existing installation, see the [_xkeys-server_ upgrade](https://gitlab.com/chris.willing/xkeys-server#upgrading) instructions.

The _node-red-contrib-xkeys\_trackball_ node itself is best installed from Node-RED's Palette manager. Go to the Palette manager's Install tab and search for _node-red-contrib-xkeys\_trackball_; then Install it once found. If not found, press the _Refresh module list_ button (two semicircular arrows) and search again.

When installed, a new _xk trackball_ will be found in the palette tab in a dedicated _Xkeys_ category.


## Usage

The full msg.payload of the _xkey\_trackball_ node is as follows:
```
{ device: NAME, pid: PID, uid: UID, index: INDEX, x: XVAL, y: YVAL }
```
where
- NAME is an abbreviated name of the source device
- PID is the Product ID of the source device
- UID is the Unit ID of the source device, typically 0 from the factory but assignable 0-255
- INDEX is the controller number (in case a device has multiple trackball controls)
- XVAL, YVAL are the x & y values emited by the device


## Issues

When configuring the node, a drop down list of possible devices shoud be available. If not, instead displaying just a note to `Press Deploy button to see device list`, first follow that advice. If that is unsuccessful (still no list of possible devices), then check the status of the xkeys-server by running the command `sudo systemctl status xkeys-server` in a terminal. Typical output of a normally running xkeys-server will be:
```
pi@pi3b:~ $ sudo systemctl status xkeys-server
● xkeys-server.service - Run xkeys-server as background service
Loaded: loaded (/etc/systemd/system/xkeys-server.service; enabled; vendor preset: enabled)
Active: active (running) since Thu 2021-10-14 09:31:55 AEST; 23h ago
```
whereas a non-running xkeys-server will show something like:
```
pi@pi3b:~ $ sudo systemctl status xkeys-server
● xkeys-server.service - Run xkeys-server as background service
Loaded: loaded (/etc/systemd/system/xkeys-server.service; enabled; vendor preset: enabled)
Active: failed (Result: timeout) since Fri 2021-10-15 08:41:37 AEST; 19s ago
```
If necessary, (re)start the xkeys-server with `sudo systemctl restart xkeys-server`


## Authors and acknowledgment
Many thanks to [P.I. Engineering](https://xkeys.com/) for the donation of several X-keys devices for development and testing.

## License
MIT
