
module.exports = function(RED) {
	var mqtt = require('mqtt');
	const connectUrl = 'mqtt://localhost';

	var httpAdminDataProducts = {};
	var httpAdminDataDevices = {};
	
	function XkeysBacklight(config) {
		RED.nodes.createNode(this, config);
		var node = this;
		node.config = config;
		//node.log("node.config = " + JSON.stringify(node.config));
		node.log("myId = " + node.config.id);

		var client = mqtt.connect(connectUrl);
		client.on('reconnect', (error) => {
			node.log('reconnecting:', error)
		})
		client.on('error', (error) => {
			node.log('Connection failed:', error)
		})
		client.on('connect', () => {
			node.log('connected')
			client.subscribe('/xkeys/server/#', function (err, granted) {
				if (!err) {
					node.log("Subscribed OK, granted: " + JSON.stringify(granted));
					client.publish('/xkeys/node', '{"request":"productList"}')
					client.publish('/xkeys/node', '{"request":"deviceList"}')
				} else {
					node.log('Subscription failed: ' + err)
				}
			})
		})
		client.on('close', () => {
			node.log("connection closed");
		})
		client.on('message', (topic, message) => {
			//console.log('received message：', topic, message.toString());
			var message_obj = "";
			try {
				message_obj = JSON.parse(message);
				//node.log("SID = " + message_obj.server_id);
				if (message_obj.request == "hello") {
					console.log(`Hello from xkeys_server at ${message_obj.server_id} - must have just (re)started `);
					// In case xkeys-server restarted with updated devices/product list
					client.publish('/xkeys/node', '{"request":"productList"}')
					client.publish('/xkeys/node', '{"request":"deviceList"}')
				}
				else if (message_obj.request == "result_deviceList") {
					// data should be a dict of info objects keyed by device's uniqueId
					var deviceList = message_obj.data;
					httpAdminDataDevices = deviceList;

					var pid_list = this.config.pid_list || "[]";
					if (device_connected(JSON.parse(pid_list), this.config.unit_id, this.config.duplicate_id)) {
						node.status( {fill:"green",shape:"dot",text:"connected"} );
					} else {
						node.status( {fill:"red",shape:"ring",text:"disconnected"} );
					}
				}
				else if (message_obj.request == "result_productList") {
					// data should be a dict of product objects
					var productList = message_obj.data;
					httpAdminDataProducts = productList;
				}
				else if (message_obj.request == "result_flashRate") { }
				else if (message_obj.request == "device_event") { }
				else if (message_obj.request == "heartbeat") {}
				else {
					node.log('Backlight received unhandled request: ' + message_obj.request);
				}
			}
			catch (e) {
				node.log('ERROR parsing message: ' + e);
			}
		})

		/*
	    * Input messages
		* We expect an internal (Backlight node's button) msg.payload of
		* { action: STARTSTOP, buttonid: ID, flashing: YESNO }
		* or external (prior node in flow) msg.payload of
		* { action: STARTSTOP, buttonid: ID, flashing: YESNO, pid_list:[e1,e2,..,eN] | pid: PID}
		* where only action is required, others optional (use config editor values)
		*/
		node.on('input', function(msg) {
			console.log("msg.payload = " + JSON.stringify(msg.payload));
			var mkeys = Object.keys(msg.payload);
			if (mkeys.includes("action")) {
				var action = msg.payload.action;
				var buttonids = [];
				var flashing = false;
				if ((action != "start") && (action != "stop")) {
					console.log("Unknown action: " + action);
					return;
				}
				if (mkeys.includes("buttonid")) {
					buttonids = msg.payload.buttonid.split(',');
				} else {
					// use configured buttonid instead
					if (node.config.buttonid != "") {
						// comma separated numbers
						buttonids = node.config.buttonid.split(',');
					} else {
						// No buttonids specified - all buttons?
					}
				}

				if (mkeys.includes("flashing")) {
					flashing = msg.payload.flashing;

				} else {
					// else use configured setting
					if (node.config.flashing) {
						flashing = (node.config.flashing.toLowerCase()=="true");
					}
				}

				// Last checks - can we uniquely identify the device?
				// "ANY" node device won't do for an internal msg
				// "ANY" node device is compulsory for an external msg
				// "Any" UID only works if only one of this PID is attached.
				var device = node.config.device;
				if (device == "ANY") {
					console.log("no device specified");
				}

				/*
				*	If both msg.payload and node.config have configured PIDs,
				*	they need to match.
				*	If neither has configured PIDs, then
				*	what is the target? All connected devices or none of them?
				*/
				var pid_list = []; // The ones we end up using
				var configured_pid_list = JSON.parse(this.config.pid_list);
				var msg_pid_list;	// What we may have been sent
				if (configured_pid_list.length == 0 ) {
					// This node has no device pid_list configured
					// so use whatever is requested in the msg.
					//console.log("No pid_list configured by this node");

					if (mkeys.includes("pid_list")) {
						// No pid_list from node
						// Some pid_list requested by msg
						msg_pid_list = msg.payload.pid_list;
						pid_list = msg_pid_list;

					} else if (mkeys.includes("pid")) {
						pid_list.push(msg.payload.pid);

					} else {
						// No pid_list from config
						// No pid_list from msg
						// Therefore do nothing
						console.log("No pid_list set");
					}
				} else {
					//console.log("Configured pid_list: " + JSON.stringify(configured_pid_list));
					if (mkeys.includes("pid_list")) {
						// Endpoints from config
						// Endpoints from msg
						// Need to arbitrate - or just ignore pid_list from msg?
						var configSet = new Set(JSON.parse(this.config.pid_list));
						if ( (msg.payload.pid_list.filter(function(x) { return configSet.has(x); })).length == 0 ) {
							// Nothing matched
							console.log("msg pid_list don't match configured pid_list. Not proceeding");
							pid_list.push(-1);
						} else {
							//console.log("msg pid_list included in configured pid_list");
							pid_list = JSON.parse(this.config.pid_list);
						}
					} else {
						// pid_list from config
						// No pid_list from msg
						// Use configured pid_list
						pid_list = JSON.parse(this.config.pid_list);
					}
				}
				if (pid_list.includes(-1)) {
					console.log("No suitable pid_list specified");
					return
				}

				/*
				*	If a unit_id has been configured, it takes precedence over any requested unit_id
				*/
				var unit_id = "";
				if (node.config.unit_id) {
					unit_id = this.config.unit_id;
					if (mkeys.includes("unit_id")) {
						if (msg.payload.unit_id != unit_id) {
							// No match
							return;
						}
					}

				} else if (mkeys.includes("unit_id")) {
					unit_id = msg.payload.unit_id;
				}

				/*
				*	If a duplicate_id has been configured, it takes precedence over any requested duplicate_id
				*/
				var duplicate_id = "";
				if (node.config.duplicate_id) {
					duplicate_id = this.config.duplicate_id;
					if (mkeys.includes("duplicate_id")) {
						if (msg.payload.duplicate_id != duplicate_id) {
							// No match
							return;
						}
					}

				} else if (mkeys.includes("duplicate_id")) {
					duplicate_id = msg.payload.duplicate_id;
				}

				/*
				*	If hue_channels has been configured, it takes precedence over any requested hue_channels
				*	and bail out if requested hue_channels clashes with configured hue_channels
				*	(although need to contend with same hue_channels being described different ways).
				*/
				var hue_channels = "";
				if (node.config.hue_channels) {
					hue_channels = this.config.hue_channels;
					if (mkeys.includes("hue_channels")) {
						if (msg.payload.hue_channels != hue_channels) {
							// No match
							return;
						}
					}
				} else if (mkeys.includes("hue_channels")) {
					hue_channels = msg.payload.hue_channels;
				}

				// Ask server to activate the backlight
				/*
				*	Message format:
				*	{request: "method", pid_list:[e0,e1,...,eN], unit_id:UID, duplicate_id:ORDER, name:METHODNAME, params: [p0,p1,...,pN]}
				*
				*	In this case, p0 is an array of button ids
				*	              p1 is hue_channels
				*	              p2 is flashing true/false
				*/
				var method_request= {	"request"      : "method",
										"pid_list"     : pid_list,
										"unit_id"      : unit_id.length==0?-1:parseInt(unit_id),
										"duplicate_id" : duplicate_id.length==0?-1:parseInt(duplicate_id),
										"name"         : buttonids.length==0?"setAllBacklights":"setBacklight",
									};
				if (action == "start") {
					method_request["params"] = [buttonids, hue_channels.length==0?"ffffff":hue_channels, flashing]
					console.log(`start: ${JSON.stringify(method_request)}`);
					client.publish('/xkeys/node', JSON.stringify(method_request));

				} else if (action == "stop") {
					method_request["params"] = [buttonids, "000000", flashing]
					console.log(`stop: ${JSON.stringify(method_request)}`);
					client.publish('/xkeys/node', JSON.stringify(method_request));

				} else {
					console.log("Unknown action requested: " + action);
				}

			} else {
				console.log("Not interested in this msg (no \"action\" field)");
			}
		})

		this.on('close', function(done) {
			client.end();
			done();
		})

		// Does any attached device match specified pids, unit_id & dup_id ?
		// pids: array of possible PIDs for a device (empty => ANY)
		// unit_id: unitId of a device
		// dup_id:  duplicate_id of a device
		function device_connected(...deviceArgs) {
			const pids = deviceArgs[0];
			const unit_id = deviceArgs[1];
			const dup_id = deviceArgs[2];
			const devs = Object.keys(httpAdminDataDevices);
			var device_matched = false;
			var regex_string = ""
			var regex;

			if (pids.length == 0) {
				//	No product_ids provided => ANY product_id
				regex_string = regex_string + "\[0-9\]+-";
				if (unit_id) {
					regex_string = regex_string + unit_id + "-";
				} else {
					regex_string = regex_string + "\[0-9\]+-";
				}
				if (dup_id) {
					regex_string = regex_string + dup_id;
				} else {
					regex_string = regex_string + "\[0-9\]+";
				}
				regex = new RegExp(regex_string);
				devs.forEach( (item) => {
					if (regex.test(item)) { device_matched = true; }
				})
			} else {
				//	An array of endpoints provided
				pids.forEach(function (item) { 
					regex_string = ""
					regex_string = regex_string + item + "-";
					if (unit_id) {
						regex_string = regex_string + unit_id + "-";
					} else {
						regex_string = regex_string + "\[0-9\]+-";
					}
					if (dup_id) {
						regex_string = regex_string + dup_id;
					} else {
						regex_string = regex_string + "\[0-9\]+";
					}
					regex = new RegExp(regex_string);
					devs.forEach( (item) => {
						if (regex.test(item)) { device_matched = true; }
					})
				})
			}
			return device_matched;

		}	// function device_connected

		/*
		// Does any attached device match configured PID & UID ?
		// pids: array of possible PIDs for a device (empty => ANY)
		// unit_id:  integer unitId of a device
		function device_connected (pids, unit_id) {
			var matched_pid;
			var matched_unit_id;
			var unit_id = unit_id;
			if (! unit_id) {
				unit_id = "ANY"
			}

			var device_connected = false;
			var devs = Object.keys(httpAdminDataDevices);
			if (devs.length > 0 ) {
				if (pids.length == 0) {
					matched_pid = "ANY";
					// Since any pid will do,
					// we just need to find one matching unit_id
					if (unit_id == "ANY") {
						matched_unit_id = "ANY";
					} else {
						devs.forEach(function (item) { 
							if (! matched_unit_id) {
								if (unit_id == httpAdminDataDevices[item].unitId) {
									matched_unit_id = httpAdminDataDevices[item].unitId;
								}
							}
						})
					}
				} else {
					// A particular PID was specified,
					// so need to find matching UID for that device
					devs.forEach(function (item) { 
						if (pids.includes(httpAdminDataDevices[item].productId)) {
							matched_pid = httpAdminDataDevices[item].productId
							if (unit_id == "ANY") {
								matched_unit_id = "ANY";
							} else {
								if (unit_id == httpAdminDataDevices[item].unitId) {
									matched_unit_id = httpAdminDataDevices[item].unitId;
								}
							}
						}
					});
				}
			} else {
				// no attached devices
				matched_pid = false;
				matched_unit_id = false;
			}
			return (pids.length==0 || pids.includes(matched_pid)) && (matched_unit_id == unit_id) ;
		}
		*/

	}	// function XkeysBacklight

	RED.nodes.registerType("xkeys_backlight", XkeysBacklight);

	RED.httpAdmin.get("/xkeys_backlight/products", function (req, res) {
		res.json(httpAdminDataProducts);
	});
	RED.httpAdmin.get("/xkeys_backlight/devices", function (req, res) {
		res.json(httpAdminDataDevices);
	});
	RED.httpAdmin.post("/xkeys_backlight_inject/:id", RED.auth.needsPermission("xkeys_backlight_inject.write"), function(req,res) {
		var node = RED.nodes.getNode(req.params.id);
		if (node != null) {
			try {
				if (req.body) {
					node.receive(req.body);
				} else {
					node.receive();
				}
				res.sendStatus(200);
			}
			catch (err) {
				res.sendStatus(500);
				node.error(RED._("inject.failed",{error:err.toString()}));
			}
		} else {
			console.log("bad post");
			res.sendStatus(404);
		}
	});

}
/*
	Node colours:
	Light blue is: R:  93, G: 200, B: 244 (#5dc8f4)
	Dark blue is:  R:  78, G: 169, B: 242 (#4ea8f2)
*/

