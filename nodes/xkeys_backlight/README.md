# node-red-contrib-xkeys_backlight

This is another in a collection of Node-RED nodes which enable access to [X-keys](https://xkeys.com/) physical devices.

It is planned to have a dedicated Node-RED node for each X-key event of interest (button, jog, joystick, etc.). These will communicate, using MQTT, with a lightweight [X-keys server](https://gitlab.com/chris.willing/xkeys-server), whose sole purpose is to mediate access to any physically attached X-keys devices.

This _xkeys\_backlight_ node encapsulates Backlight operations of enabling or disabling the Hue Cannels for any or specific X-keys devices.


## Installation

This node requires _xkeys-server_ version 0.0.19 to be running. Please follow the instructions at the [_xkeys-server_](https://gitlab.com/chris.willing/xkeys-server) development repository to install it or, to upgrade an existing installation, see the [_xkeys-server_ upgrade](https://gitlab.com/chris.willing/xkeys-server#upgrading) instructions.

The _node-red-contrib-xkeys\_backlight_ node itself is best installed from Node-RED's Palette manager. Go to the Palette manager's Install tab and search for _node-red-contrib-xkeys\_backlight_; then Install it once found. If not found, press the _Refresh module list_ button (two semicircular arrows) and search again.

When installed, a new _xk Backlight_ node will be found in the palette tab in the dedicated _Xkeys_ category.


## Usage

The node's configuration editor can be used to set the target Xkeys device, as well as the target button to operate on. The target button set in the configuration editor may be overridden by settings the relevant field in the msg.payload of an incoming node. A msg.payload containing only an _action_ field activates the target button with whatever settings are currently configured in the configuration editor.
The msg.payload expected by the _xkey\_backlight_ node is as follows:
```
{ action: STARTSTOP, buttonid: ID, flashing: YESNO, pid_list: [e1,e2,..,eN] | pid: PID, hue_channels: CHANNELS}
```
where
- STARTSTOP is either of *"start"* or *"stop"*. This is the only required field. All other fields are optional and, if not provided, the configured settings of the node are used.
- ID indicates which button(s) to operate on. Multiple buttons may be targeted by entering comma separated ID numbers in this field e.g. 1,3,6,7 whereas an empty buttonid value indicates that _all_ available backlights should be targeted.
- YESNO is one of _true_ or _false_
- PID is the product id of the device. Alternatively and array of device PIDs may be specified instead using the _pid_list_ field.
- CHANNELS is a string describing the which channels will be active e.g. ff0000 => red, 0000ff => blue. Not all devices are capable of all hues.

An example flow is provided in the _examples_ directory to demonstrate activation of the _xkeys\_backlight_ node.
<p align="center" width="100%" ><img width="70%" src="examples/xkeys_backlight-test.png" ></p>

- the first method is the toggle switch attached to the node itself. In the configuration shown (no device selected, no button ids selected), the toggle switch will start/stop the the backlights of all keys on all attached devices.
- a second method is to use _Inject_ nodes with appropriately crafted msg.payloads.
- another method uses an _xkeys\_button_ node to activate the _xkeys\_backlight_ node by use of a _function_ node (labeled _Button to Backlight_) which is configured to use the _button_ node's "down" and "up" actions to activate the red and blue channels respecitvely.

Note the use of an _xkeys\_intensity_ node set the intensity of each channel. In this case the blue intensity is set quite low, the red intensity is set quite high. If the _xkeys\_backlight_ node has been initialized by the _Start backlights_ node, all keys will have a low blue hue. As the device's buttons are pressed, they will turn and remain red until the button is released whereupon the buttons revert to the initial low blue hue.



## Issues

When configuring the node, a drop down list of possible devices shoud be available. If not, instead displaying just a note to `Press Deploy button to see device list`, first follow that advice. If that is unsuccessful (still no list of possible devices), then check the status of the xkeys-server by running the command `sudo systemctl status xkeys-server` in a terminal. Typical output of a normally running xkeys-server will be:
```
pi@pi3b:~ $ sudo systemctl status xkeys-server
● xkeys-server.service - Run xkeys-server as background service
Loaded: loaded (/etc/systemd/system/xkeys-server.service; enabled; vendor preset: enabled)
Active: active (running) since Thu 2021-10-14 09:31:55 AEST; 23h ago
```
whereas a non-running xkeys-server will show something like:
```
pi@pi3b:~ $ sudo systemctl status xkeys-server
● xkeys-server.service - Run xkeys-server as background service
Loaded: loaded (/etc/systemd/system/xkeys-server.service; enabled; vendor preset: enabled)
Active: failed (Result: timeout) since Fri 2021-10-15 08:41:37 AEST; 19s ago
```
If necessary, (re)start the xkeys-server with `sudo systemctl restart xkeys-server`


## Authors and acknowledgment
Many thanks to [P.I. Engineering](https://xkeys.com/) for the donation of several X-keys devices for development and testing.

## License
MIT
