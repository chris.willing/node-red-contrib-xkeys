
module.exports = function(RED) {
	var mqtt = require('mqtt');
	const connectUrl = 'mqtt://localhost';
	const qos = 0;

	var httpAdminDataProducts = {};
	var httpAdminDataDevices = {};
	var httpAdminDataIntensity = {};
	
	function XkeysIntensity(config) {
		RED.nodes.createNode(this, config);
		var node = this;
		node.config = config;
		//node.log("node.config = " + JSON.stringify(node.config));
		node.log("myId = " + node.config.id);

		var client = mqtt.connect(connectUrl);
		client.on('reconnect', (error) => {
			node.log('reconnecting:', error)
		})
		client.on('error', (error) => {
			node.log('Connection failed:', error)
		})
		client.on('connect', () => {
			node.log('connected')
			client.subscribe({'/xkeys/server/#':{qos:qos}}, function (err, granted) {
				if (!err) {
					node.log("Subscribed OK, granted: " + JSON.stringify(granted));
					client.publish('/xkeys/node', '{"request":"productList"}')
					client.publish('/xkeys/node/xkeys_intensity', '{"request":"deviceList"}')
				} else {
					node.log('Subscription failed: ' + err)
				}
			})
		})
		client.on('close', () => {
			node.log("connection closed");
		})
		client.on('message', (topic, message) => {
			//node.log(`received topic：${topic}, msg: ${message.toString()}`);
			var message_obj = "";
			try {
				message_obj = JSON.parse(message);
				//node.log("SID = " + message_obj.server_id);
				if (message_obj.request == "hello") {
					console.log(`Hello from xkeys_server at ${message_obj.server_id} - must have just (re)started `);
				}
				else if (message_obj.request == "result_deviceList") {
					// data should be a dict of info objects keyed by device's uniqueId
					var deviceList = message_obj.data;
					httpAdminDataDevices = deviceList;

					// Based on attached deviceList, are we connected?
					var pid_list = this.config.pid_list || "[]";
					if (device_connected(JSON.parse(pid_list), this.config.unit_id, this.config.duplicate_id)) {
						node.status( {fill:"green",shape:"dot",text:"connected"} );
					} else {
						node.status( {fill:"red",shape:"ring",text:"disconnected"} );
					}
				}
				else if (message_obj.request == "result_productList") {
					// data should be a dict of product objects
					var productList = message_obj.data;
					httpAdminDataProducts = productList;
				}
				else if (message_obj.request == "device_event") { }
				else if (message_obj.request == "result_flashRate") {}
				else if (message_obj.request == "heartbeat") {}
				else {
					node.log('Intensity node received unhandled request: ' + message_obj.request);
				}
			}
			catch (e) {
				node.log('ERROR parsing message: ' + e);
			}
		})

		/*
	    * Input messages
		* We expect a msg.payloads of at least:
		*	{"intensity": VAL}
		* in which case the node's configured pid_list & uinitid values are used
		*
		* The usual (internally configured) msg.payload will have:
		*	{"intensity": [blueVal,redVal], "pid_list": [e0,e1,e2,...eN],"unit_id": UID}
		*	or
		*	{"intensity": [blueVal,redVal], "pid": PID,"unit_id": UID}
		*/
		node.on('input', function(msg) {
			// Don't bother checking anything if we're not representing any device at all
			if (! device_connected(JSON.parse(node.config.pid_list), this.config.unit_id, this.config.duplicate_id)) {
				return;
			}

			var mkeys = Object.keys(msg.payload);
			node.log("msg.payload: " + JSON.stringify(msg.payload));
			console.log("mkeys: " + JSON.stringify(mkeys));
			if (mkeys.includes("intensity")) {
				var intensity = JSON.parse(msg.payload.intensity);
				// Check that intensity is OK
				if( (isNaN(intensity[0])) || (intensity[0] < 0) || (intensity[0] > 255) ) {
					node.log("Received bad blue intensity value: " + msg.payload.intensity);
					return;
				}
				if( (isNaN(intensity[1])) || (intensity[1] < 0) || (intensity[1] > 255) ) {
					node.log("Received bad red intensity value: " + msg.payload.intensity);
					return;
				}

				// Check for valid pid_list
				var request_pid_list = "[]";
				if (JSON.parse(node.config.pid_list).length == 0) {
					//msg.payload can set pid_list
					if (mkeys.includes("pid_list")) {
						request_pid_list = msg.payload.pid_list;

					} else if (mkeys.includes("pid")) {
						// Test value first?
						var request_pid = JSON.parse(request_pid_list);
						request_pid.push(msg.payload.pid);
						request_pid_list = JSON.stringify(request_pid);

					}

				} else {
					//msg.payload.pid_list (or pid) must match configured pid_list
					if (mkeys.includes("pid_list")) {
						// For now, just try simple string match for pid_list
						// Later check if pid_list are differently ordered
						if (msg.payload.pid_list == node.config.pid_list ) {
							request_pid_list = msg.payload.pid_list;
						} else {
							node.log("Incoming pid_list don't match configured pid_list");
							return;
						}
					} else if (mkeys.includes("pid")) {
						if (JSON.parse(node.config.pid_list).includes(parseInt(msg.payload.pid))) {
							request_pid_list = node.config.pid_list;
						} else {
							node.log("Incoming pid doesn't match configured pid_list");
							return;
						}
					} else {
						request_pid_list = node.config.pid_list;
					}
				}

				// Check for valid unit_id
				var requested_unit_id = node.config.unit_id;
				if (node.config.unit_id.length == 0) {
					if (mkeys.includes("unit_id")) {
						requested_unit_id = msg.payload.unit_id;
					}
				} else {
					if (mkeys.includes("unit_id")) {
						if (msg.payload.unit_id != node.config.unit_id) {
							node.log("msg.payload.unit_id doesn't match configured unit_id");
							return;
						}
					}
				}

				// Check for valid duplicate_id
				var requested_duplicate_id = node.config.duplicate_id;
				if (node.config.duplicate_id.length == 0) {
					if (mkeys.includes("duplicate_id")) {
						requested_duplicate_id = msg.payload.duplicate_id;
					}
				} else {
					if (mkeys.includes("duplicate_id")) {
						if (msg.payload.duplicate_id != node.config.duplicate_id) {
							node.log("msg.payload.duplicate_id doesn't match configured duplicate_id");
							return;
						}
					}
				}

				// Ask server to set the intensity
				/*
				*	Message format:
				*	{request: "method", pid_list:[e1,e2,...,eN], unit_id:UID, duplicate_id:ORDER, name:METHODNAME, params: [p1,p2,...,pN]}
				*	where p1 is an array of blue/red intensity values to be set
				*/
				var method_request;
				method_request = {	"request"      : "method",
									"pid_list"     : JSON.parse(request_pid_list),
									"unit_id"      : requested_unit_id.length==0?-1:parseInt(requested_unit_id.length),
									"duplicate_id" : requested_duplicate_id.length==0?-1:parseInt(requested_duplicate_id),
									"name"         : "setBacklightIntensity",
									"params"       : [intensity]
								};
				//console.log('Requesting: ' + JSON.stringify(method_request));
				client.publish('/xkeys/node/xkeys_intensity', JSON.stringify(method_request));

			} else {
				console.log("Not interested in this msg (no \"intensity\" field)");
			}
		})

		this.on('close', function(done) {
			client.end();
			done();
		})

		// Does any attached device match specified pids, unit_id & dup_id ?
		// pids: array of possible PIDs for a device (empty => ANY)
		// unit_id: unitId of a device
		// dup_id:  duplicate_id of a device
		function device_connected(...deviceArgs) {
			const pids = deviceArgs[0];
			const unit_id = deviceArgs[1];
			const dup_id = deviceArgs[2];
			const devs = Object.keys(httpAdminDataDevices);
			var device_matched = false;
			var regex_string = ""
			var regex;

			if (pids.length == 0) {
				//	No product_ids provided => ANY product_id
				regex_string = regex_string + "\[0-9\]+-";
				if (unit_id) {
					regex_string = regex_string + unit_id + "-";
				} else {
					regex_string = regex_string + "\[0-9\]+-";
				}
				if (dup_id) {
					regex_string = regex_string + dup_id;
				} else {
					regex_string = regex_string + "\[0-9\]+";
				}
				regex = new RegExp(regex_string);
				devs.forEach( (item) => {
					if (regex.test(item)) { device_matched = true; }
				})
			} else {
				//	An array of endpoints provided
				pids.forEach(function (item) { 
					regex_string = ""
					regex_string = regex_string + item + "-";
					if (unit_id) {
						regex_string = regex_string + unit_id + "-";
					} else {
						regex_string = regex_string + "\[0-9\]+-";
					}
					if (dup_id) {
						regex_string = regex_string + dup_id;
					} else {
						regex_string = regex_string + "\[0-9\]+";
					}
					regex = new RegExp(regex_string);
					devs.forEach( (item) => {
						if (regex.test(item)) { device_matched = true; }
					})
				})
			}
			return device_matched;

		}	// function device_connected

	}	// function XkeysIntensity

	RED.nodes.registerType("xkeys_intensity", XkeysIntensity);

	RED.httpAdmin.get("/xkeys_intensity/products", function (req, res) {
		res.json(httpAdminDataProducts);
	});
	RED.httpAdmin.get("/xkeys_intensity/devices", function (req, res) {
		res.json(httpAdminDataDevices);
	});

	RED.httpAdmin.post("/xkeys_intensity_inject/:id", RED.auth.needsPermission("xkeys_intensity_inject.write"), function(req,res) {
		console.log("posting something: " + JSON.stringify(req.body));
		var node = RED.nodes.getNode(req.params.id);
		if (node != null) {
			try {
				if (req.body) {
					node.receive(req.body);
				} else {
					node.receive();
				}
				res.sendStatus(200);
			}
			catch (err) {
				res.sendStatus(500);
				node.error(RED._("inject.failed",{error:err.toString()}));
			}
		} else {
			console.log("bad post");
			res.sendStatus(404);
		}
	});

}
/*
	Node colours:
	Light blue is: R:  93, G: 200, B: 244 (#5dc8f4)
	Dark blue is:  R:  78, G: 169, B: 242 (#4ea8f2)
*/

