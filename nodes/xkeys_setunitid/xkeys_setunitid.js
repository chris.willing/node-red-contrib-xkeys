
module.exports = function(RED) {
	var mqtt = require('mqtt');
	const connectUrl = 'mqtt://localhost';
	const qos = 0;

	var httpAdminDataProducts = {};
	var httpAdminDataDevices = {};
	var httpAdminDataSetUnitID = {};
	
	function XkeysSetUnitID(config) {
		RED.nodes.createNode(this, config);
		var node = this;
		node.config = config;
		//node.log("node.config = " + JSON.stringify(node.config));
		node.log("myId = " + node.config.id);

		var client = mqtt.connect(connectUrl);
		client.on('reconnect', (error) => {
			node.log('reconnecting:', error)
		})
		client.on('error', (error) => {
			node.log('Connection failed:', error)
		})
		client.on('connect', () => {
			node.log('connected')
			client.subscribe({'/xkeys/server/#':{qos:qos}}, function (err, granted) {
				if (!err) {
					node.log("Subscribed OK, granted: " + JSON.stringify(granted));
					client.publish('/xkeys/node', '{"request":"productList"}')
					client.publish('/xkeys/node/xkeys_setunit_id', '{"request":"deviceList"}')
				} else {
					node.log('Subscription failed: ' + err)
				}
			})
		})
		client.on('close', () => {
			node.log("connection closed");
		})
		client.on('message', (topic, message) => {
			//node.log(`received topic：${topic}, msg: ${message.toString()}`);
			var message_obj = "";
			try {
				message_obj = JSON.parse(message);
				//node.log("SID = " + message_obj.sid);
				if (message_obj.request == "hello") {
					console.log(`Hello from xkeys_server at ${message_obj.sid} - must have just (re)started `);
				}
				else if (message_obj.request == "result_deviceList") {
					// data should be a dict of info objects keyed by device's uniqueId
					var deviceList = message_obj.data;
					httpAdminDataDevices = deviceList;

					// Based on attached deviceList, are we connected?
					var pid_list = this.config.pid_list || "[]";
					if (device_connected(JSON.parse(pid_list), this.config.unit_id, this.config.duplicate_id)) {
						node.status( {fill:"green",shape:"dot",text:"connected"} );
					} else {
						node.status( {fill:"red",shape:"ring",text:"disconnected"} );
					}
				}
				else if (message_obj.request == "result_productList") {
					// data should be a dict of product objects
					var productList = message_obj.data;
					httpAdminDataProducts = productList;
				}
				else if (message_obj.request == "result_productList") { }
				else if (message_obj.request == "result_deviceList") { }
				else if (message_obj.request == "device_event") { }
				else if (message_obj.request == "result_flashRate") {}
				else if (message_obj.request == "heartbeat") {}
				else {
					node.log('SetUnitID received unhandled request: ' + message_obj.request);
				}
			}
			catch (e) {
				node.log('ERROR parsing message: ' + e);
			}
		})

		/*
	    * Input messages
		* We expect a msg.payloads of
		*
		*/
		node.on('input', function(msg) {
			//console.log("INPUT: " + JSON.stringify(msg));
			var mkeys = Object.keys(msg.payload);
			//console.log("mkeys: " + JSON.stringify(mkeys));
			//console.log("msg.payload: " + JSON.stringify(msg.payload));
			if (mkeys.includes("new_unit_id")) {
				/*
					Check that the request is OK. In particular,
					ensure that target UID is not already used by another device with same PID
				*/
				var valid_target = true;
				try {
					JSON.parse(msg.payload.pid_list).forEach( (pid) => {
						var unique_id = pid + "_" + msg.payload.new_unit_id;
						if (Object.keys(httpAdminDataDevices).includes(unique_id)) {
							valid_target = false;
						}
					});
				}
				catch (err) {
					console.log("Error parsing pid_list: " + err);
					valid_target = false;
				}
				if (!valid_target) { return; }

				var unit_id = this.config.unit_id
				if( unit_id.length == 0) {
					// Can't proceed without current unit_id
					node.log("Can't proceed without current unit_id");
					return;
				}
				var duplicate_id = this.config.duplicate_id
				if( duplicate_id.length == 0) {
					// Can't proceed without current duplicate_id
					node.log("Can't proceed without duplicate_id unit_id");
					return;
				}

				// Ask server to change the Unit ID
				/*
				*	Message format:
				*	{request: "method", pid_list:[e0,e1,e2,...,eN], uid:UID, name:METHODNAME, params: [p0,p1,...,pN]}
				*	where p0 is an empty, not used, array in this case
				*/
				var method_request = {	"request"  : "method",
									"pid_list"     : JSON.parse(msg.payload.pid_list),
									"unit_id"      : unit_id.length==0?-1:parseInt(unit_id),
									"duplicate_id" : duplicate_id.length==0?-1:parseInt(duplicate_id),
									"name"         : "setUnitID",
									"params"       : [[], msg.payload.new_unit_id]
								} || {};
				client.publish('/xkeys/node/xkeys_setunit_id', JSON.stringify(method_request));

			} else {
				console.log("Not interested in this msg (no \"new_unit_id\" field)");
			}
		})

		this.on('close', function(done) {
			client.end();
			done();
		})

		// Does any attached device match specified pids, unit_id & dup_id ?
		// pids: array of possible PIDs for a device (empty => ANY)
		// unit_id: unitId of a device
		// dup_id:  duplicate_id of a device
		function device_connected(...deviceArgs) {
			const pids = deviceArgs[0];
			const unit_id = deviceArgs[1];
			const dup_id = deviceArgs[2];
			const devs = Object.keys(httpAdminDataDevices);
			var device_matched = false;
			var regex_string = ""
			var regex;

			if (pids.length == 0) {
				//	No product_ids provided => ANY product_id
				regex_string = regex_string + "\[0-9\]+-";
				if (unit_id) {
					regex_string = regex_string + unit_id + "-";
				} else {
					regex_string = regex_string + "\[0-9\]+-";
				}
				if (dup_id) {
					regex_string = regex_string + dup_id;
				} else {
					regex_string = regex_string + "\[0-9\]+";
				}
				regex = new RegExp(regex_string);
				devs.forEach( (item) => {
					if (regex.test(item)) { device_matched = true; }
				})
			} else {
				//	An array of endpoints provided
				pids.forEach(function (item) { 
					regex_string = ""
					regex_string = regex_string + item + "-";
					if (unit_id) {
						regex_string = regex_string + unit_id + "-";
					} else {
						regex_string = regex_string + "\[0-9\]+-";
					}
					if (dup_id) {
						regex_string = regex_string + dup_id;
					} else {
						regex_string = regex_string + "\[0-9\]+";
					}
					regex = new RegExp(regex_string);
					devs.forEach( (item) => {
						if (regex.test(item)) { device_matched = true; }
					})
				})
			}
			return device_matched;

		}	// function device_connected

	}	// function XkeysSetUnitID

	RED.nodes.registerType("xkeys_setunitid", XkeysSetUnitID);

	RED.httpAdmin.get("/xkeys_setunitid/products", function (req, res) {
		res.json(httpAdminDataProducts);
	});
	RED.httpAdmin.get("/xkeys_setunitid/devices", function (req, res) {
		res.json(httpAdminDataDevices);
	});

	RED.httpAdmin.post("/xkeys_setunit_id_inject/:id", RED.auth.needsPermission("xkeys_setunit_id_inject.write"), function(req,res) {
		//console.log("posting something: " + JSON.stringify(req.body));
		var node = RED.nodes.getNode(req.params.id);
		if (node != null) {
			try {
				if (req.body) {
					node.receive(req.body);
				} else {
					node.receive();
				}
				res.sendStatus(200);
			}
			catch (err) {
				res.sendStatus(500);
				node.error(RED._("inject.failed",{error:err.toString()}));
			}
		} else {
			console.log("bad post");
			res.sendStatus(404);
		}
	});

}
/*
	Node colours:
	Light blue is: R:  93, G: 200, B: 244 (#5dc8f4)
	Dark blue is:  R:  78, G: 169, B: 242 (#4ea8f2)
*/

