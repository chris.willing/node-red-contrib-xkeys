# node-red-contrib-xkeys

A group of Node-RED nodes to enable access to X-keys physical devices.

It is planned to develop a dedicated Node-RED node for each X-key event of interest (button, jog, joystick, etc.). These nodes will communicate, using [MQTT](https://www.npmjs.com/package/mqtt), with a lightweight [X-keys server](https://gitlab.com/chris.willing/xkeys-server), whose sole purpose is to mediate access to any physically attached X-keys devices, via the [SuperFlyTv xkeys library](https://github.com/SuperFlyTV/xkeys).


## Available xkeys event nodes

The xkeys event nodes available so far are:
- [_xkeys\_button_](nodes/xkeys_button)
- [_xkeys\_jogshuttle_](nodes/xkeys_jogshuttle)
- [_xkeys\_joystick_](nodes/xkeys_joystick)
- [_xkeys\_tbar_](nodes/xkeys_tbar)
- [_xkeys\_rotary_](nodes/xkeys_rotary)
- [_xkeys\_trackball_](nodes/xkeys_trackball)
- [_xkeys\_lcd_](nodes/xkeys_lcd)
- [_xkeys\_led_](nodes/xkeys_led)
- [_xkeys\_flashrate_](nodes/xkeys_flashrate)
- [_xkeys\_backlight_](nodes/xkeys_backlight)
- [_xkeys\_intensity_](nodes/xkeys_intensity)
- [_xkeys\_savebacklights_](nodes/xkeys_savebacklights)
- [_xkeys\_setunitid_](nodes/xkeys_setunitid)
- [_xkeys\_writeData_](nodes/xkeys_writeData)


## Authors and acknowledgment
Many thanks to [P.I. Engineering](https://xkeys.com/) for the donation of several X-keys devices for development and testing.

## License
MIT
